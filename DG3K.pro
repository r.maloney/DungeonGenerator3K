#-------------------------------------------------
#
# Project created by QtCreator 2011-01-03T10:52:42
#
#-------------------------------------------------

QT       += core gui

TARGET = DG3K
TEMPLATE = app


SOURCES += main.cpp\
    Dungeon.cpp \
    MainWindow.cpp \
    DungeonGenerator.cpp \
    HexTileCreator.cpp \
    ControlPanel.cpp \
    Section.cpp \
    Room.cpp \
    Node.cpp

HEADERS  += \
    Dungeon.h \
    MainWindow.h \
    DungeonGenerator.h \
    HexTileCreator.h \
    ControlPanel.h \
    Section.h \
    Room.h \
    Node.h

FORMS    += \
    MainWindow.ui
