#include "DungeonGenerator.h"
#include <stdlib.h>
#include <QDebug>
#include "Section.h"
#include "Room.h"
#include <vector>
#include <stack>
#include "Node.h"

using namespace std;

DungeonGenerator::DungeonGenerator()
{
}

void DungeonGenerator::BinarySpacePartitioning(Dungeon* dungeon)
{
    if (0 == dungeon->GetWidth() || 0 == dungeon->GetMaxRoomWidth()
        || 0 == dungeon->GetHeight() || 0 == dungeon->GetMaxRoomHeight()  )
        {
        return;
    }

    Node* rootNode = new Node();
    Section* rootSection = new Section();
    rootSection->SetHeight(dungeon->GetHeight());
    rootSection->SetWidth(dungeon->GetWidth());
    rootNode->SetSection(rootSection);
    SplitSection(rootNode, dungeon->GetMinRoomWidth(), dungeon->GetMinRoomHeight());
    PopulateDungeonFromTree(rootNode, dungeon);
    TunnelBetweenRooms(3, rootNode, dungeon);
    delete rootNode;
}

void DungeonGenerator::SplitSection(Node* rootNode, int specifiedMinRoomWidth, int specifiedMinRoomHeight)
{
    //In order traversal
    stack<Node*> traversalStack;
    traversalStack.push(rootNode);

    //Create each section as though the room were two squares wider and higher than it is
    //so the room can be drawn in the middle, leaving at least a one square gap around rooms
    //next to each other as a result of BSP;
    int minRoomWidth = specifiedMinRoomWidth + 2;
    int minRoomHeight = specifiedMinRoomHeight + 2;

    while (traversalStack.size() > 0)
    {
        Node* topNode = traversalStack.top();
        qDebug() << "POP!";
        traversalStack.pop();

        qDebug() << "###############NEW ITERATION#####################";
        bool verticalCut = topNode->IsVerticalCut();
        Section* section = topNode->GetSection();
        qDebug() << "Splitting node containing a section that is " << section->GetWidth() << " wide and " << section->GetHeight() << " high.";

        if (verticalCut)
        {
            //First ensure it is even possible to divide.
            if ((2 * minRoomWidth) > section->GetWidth())
            {
                continue;
            }
        }
        else
        {
            //First ensure it is even possible to divide.
            if ((2 * minRoomHeight) > section->GetHeight())
            {
                continue;
            }
        }

        int splitMin = 0;
        int splitMax = 0;

        if (verticalCut)
        {
            splitMin = section->GetUpperLeftX() + minRoomWidth; //Split has to be at least the minium number of squares from left edge.
            splitMax = (section->GetUpperLeftX() + section->GetWidth()) - minRoomWidth; //Split has to be at least the minium number of squares from right edge.
        }
        else
        {
            splitMin = section->GetUpperLeftY() + minRoomHeight; //Split has to be at least the minium number of squares from left edge.
            splitMax = (section->GetUpperLeftY() + section->GetHeight()) - minRoomWidth; //Split has to be at least the minium number of squares from right edge.
        }

        int actualSplit = 0;

        if (splitMin == splitMax)
        {
            actualSplit = splitMin;
        }
        else
        {
            actualSplit = (rand() % (splitMax - splitMin + 1)) + splitMin;
        }

        qDebug() << "Split min/max is " << splitMin << "/" << splitMax << " and actual split is " << actualSplit;

        Section* leftSection = new Section();

        if (verticalCut)
        {
            leftSection->SetUpperLeftX(section->GetUpperLeftX());
            leftSection->SetUpperLeftY(section->GetUpperLeftY());
            leftSection->SetWidth(actualSplit - section->GetUpperLeftX());
            leftSection->SetHeight(section->GetHeight());
            qDebug() << "Cut section vertically, upper left is at ("
                    << leftSection->GetUpperLeftX() << ","
                    << leftSection->GetUpperLeftY() << ") and it is"
                    << leftSection->GetWidth() << " wide and "
                    << leftSection->GetHeight() << " high.";

        }
        else
        {
            leftSection->SetUpperLeftX(section->GetUpperLeftX());
            leftSection->SetUpperLeftY(section->GetUpperLeftY());
            leftSection->SetWidth(section->GetWidth());
            leftSection->SetHeight(actualSplit - section->GetUpperLeftY());
            qDebug() << "Cut section horizontally, upper left is at ("
                    << leftSection->GetUpperLeftX() << ","
                    << leftSection->GetUpperLeftY() << ") and it is"
                    << leftSection->GetWidth() << " wide and "
                    << leftSection->GetHeight() << " high.";
        }

        Section* rightSection = new Section();

        if (verticalCut)
        {
            rightSection->SetUpperLeftX(actualSplit);
            rightSection->SetUpperLeftY(section->GetUpperLeftY());
            rightSection->SetWidth((section->GetUpperLeftX() + section->GetWidth()) - actualSplit);
            rightSection->SetHeight(section->GetHeight());
            qDebug() << "Cut section vertically, upper left is at ("
                    << rightSection->GetUpperLeftX() << ","
                    << rightSection->GetUpperLeftY() << ") and it is"
                    << rightSection->GetWidth() << " wide and "
                    << rightSection->GetHeight() << " high.";
        }
        else
        {
            rightSection->SetUpperLeftX(section->GetUpperLeftX());
            rightSection->SetUpperLeftY( actualSplit);
            rightSection->SetWidth(section->GetWidth());
            rightSection->SetHeight((section->GetUpperLeftY() + section->GetHeight()) - actualSplit);
            qDebug() << "Cut section horizontally, upper left is at ("
                    << rightSection->GetUpperLeftX() << ","
                    << rightSection->GetUpperLeftY() << ") and it is"
                    << rightSection->GetWidth() << " wide and "
                    << rightSection->GetHeight() << " high.";
        }

        //Toggle whether we are cutting vertically or horizontally.
        verticalCut = !verticalCut;
        Node* leftNode = new Node();
        leftNode->SetSection(leftSection);
        leftNode->SetVerticalCut(verticalCut);

        Node* rightNode = new Node();
        rightNode->SetSection(rightSection);
        rightNode->SetVerticalCut(verticalCut);

        topNode->SetFirstChild(leftNode);
        topNode->SetSecondChild(rightNode);

        traversalStack.push(rightNode);
        traversalStack.push(leftNode);
    }
}

void DungeonGenerator::PopulateDungeonFromTree(Node* rootNode, Dungeon* dungeon)
{
    qDebug() << "Populating dungeon from a tree..." << endl;
    int roomsCreated = 0;

    //In order traversal
    stack<Node*> traversalStack;
    if (NULL != rootNode->GetFirstChild())
    {
        traversalStack.push(rootNode->GetSecondChild());
        traversalStack.push(rootNode->GetFirstChild());
    }

    qDebug() << "Traversal Stack size is " << traversalStack.size() << endl;

    while(traversalStack.size() > 0)
    {
        if (roomsCreated >= dungeon->GetMaxNumberOfRooms())
        {
            qDebug() << "Created the max number of rooms!" << endl;
            break;
        }

        Node* topNode = traversalStack.top();
        if (NULL != topNode->GetFirstChild())
        {
            qDebug() << "Top has a first child!" << endl;
            traversalStack.pop();

            if (NULL != topNode->GetSecondChild())
            {
                traversalStack.push(topNode->GetSecondChild());
            }
            else
            {
                qDebug() << "For some reason the second child is NULL even though the first is not." << endl;
            }

            traversalStack.push(topNode->GetFirstChild());
            continue;
        }

        //No uneven splits so a node either has two populated children or none at all.
        //Thus here we've hit a leaf node.        
        if (PopulateSectionWithRoom(topNode->GetSection(), dungeon, 1))
        {
            roomsCreated++;
        }

        traversalStack.pop();
    }
}

void DungeonGenerator::ConstantSizePartitioning(Dungeon* dungeon)
{
    if (0 == dungeon->GetWidth() || 0 == dungeon->GetMaxRoomWidth()
        || 0 == dungeon->GetHeight() || 0 == dungeon->GetMaxRoomHeight()  )
        {
        return;
    }

    vector<Section*> sections;

    //Add 1 to each to ensure each room is separate by at least one tile.
    for (int x = 0; x < dungeon->GetWidth(); x+=dungeon->GetMaxRoomWidth() + 1)
    {
        for (int y = 0; y < dungeon->GetHeight(); y+=dungeon->GetMaxRoomHeight() + 1)
        {
            Section* section = new Section();
            section->SetUpperLeftX(x);
            section->SetUpperLeftY(y);
            if (x + dungeon->GetMaxRoomWidth() > dungeon->GetWidth())
            {
                section->SetWidth(dungeon->GetWidth() - x);
            }
            else
            {
                section->SetWidth(dungeon->GetMaxRoomWidth());
            }

            if (y + dungeon->GetMaxRoomHeight() > dungeon->GetHeight())
            {
                section->SetHeight(dungeon->GetHeight() - y);
            }
            else
            {
                section->SetHeight(dungeon->GetMaxRoomHeight());
            }

            qDebug() << "Adding section with upper left at (" << x << ", " << y << ")";
            qDebug() << "Section is (" << section->GetWidth() << " x " << section->GetHeight() << ")";
            sections.push_back(section);
        }
    }

    if (sections.size() > 0)
    {
        unsigned int numberOfAttempts = 0;
        int numberRoomsToBuild = dungeon->GetMaxNumberOfRooms();

        while (numberRoomsToBuild > 0)
        {
            if (numberOfAttempts++ > sections.size())
            {
                break;
            }

            bool suitableSectionFound = false;
            unsigned int attempts = 0;

            while (!suitableSectionFound)
            {
                int indexFromRandomLottery = rand() % sections.size();

                if (NULL == sections[indexFromRandomLottery]->GetRoom())
                {
                    if (PopulateSectionWithRoom(sections[indexFromRandomLottery], dungeon))
                    {
                        numberRoomsToBuild--;
                        suitableSectionFound = true;
                    }
                }
                if(attempts++ > sections.size())
                {
                    suitableSectionFound = true;
                }
            }
        }
    }
}

bool DungeonGenerator::PopulateSectionWithRoom(Section* section, Dungeon* dungeon)
{
    return PopulateSectionWithRoom(section, dungeon, 0);
}

bool DungeonGenerator::PopulateSectionWithRoom(Section* section, Dungeon* dungeon, int offsetFromSection)
{
    bool added = false;

    Room* room = new Room();

    int roomWidth = 0;
    int roomHeight = 0;

    if ( dungeon->GetMaxRoomWidth() == dungeon->GetMinRoomWidth())
    {
        roomWidth = dungeon->GetMaxRoomWidth();
    }
    else
    {
        while (roomWidth < dungeon->GetMinRoomWidth())
        {
            roomWidth = rand() % dungeon->GetMaxRoomWidth();
        }
    }

    if ( dungeon->GetMaxRoomHeight() == dungeon->GetMinRoomHeight())
    {
        roomHeight = dungeon->GetMaxRoomHeight();
    }
    else
    {
        while (roomHeight < dungeon->GetMinRoomHeight())
        {
            roomHeight = rand() % dungeon->GetMaxRoomHeight();
        }
    }

    if ((roomWidth + (2 * offsetFromSection)) > section->GetWidth())
    {
        roomWidth -= (2 * offsetFromSection);
    }

    if ((roomHeight + (2 * offsetFromSection))  > section->GetHeight())
    {
        roomHeight -= (2 * offsetFromSection);
    }

    room->SetWidth(roomWidth);
    room->SetHeight(roomHeight);
    room->SetUpperLeftX(section->GetUpperLeftX()+offsetFromSection);
    room->SetUpperLeftY(section->GetUpperLeftY()+offsetFromSection);
    qDebug() << "Created room of width and height " << roomWidth << "x" << roomHeight << " Upper left is " << room->GetUpperLeftX() << "," << room->GetUpperLeftY() << endl;
    section->SetRoom(room);
    PopulateDungeonFromSection(section, dungeon, offsetFromSection);
    added = true;

    return added;
}

void DungeonGenerator::PopulateDungeonFromSection(Section* section, Dungeon* dungeon)
{
    PopulateDungeonFromSection(section, dungeon, 0);
}

void DungeonGenerator::PopulateDungeonFromSection(Section* section, Dungeon* dungeon, int offsetFromSection)
{
    if (NULL != section->GetRoom())
    {
        int upperLeftX = section->GetUpperLeftX() + offsetFromSection;
        int upperLeftY = section->GetUpperLeftY() + offsetFromSection;
        int upperRightX = upperLeftX + section->GetRoom()->GetWidth();
        int lowerLeftY = upperLeftY+ section->GetRoom()->GetHeight();

        for (int x = upperLeftX; x < upperRightX; x++)
        {
            for (int y = upperLeftY; y < lowerLeftY; y++)
            {
                qDebug() << "Placing room point at (" << x <<", " << y << ")";
                if (x == upperLeftX || x == (upperRightX - 1))
                {
                    (*dungeon)[x][y] = 'W';
                }
                else
                {
                    if (y == upperLeftY || y == (lowerLeftY - 1))
                    {
                        (*dungeon)[x][y] = 'W';
                    }
                    else
                    {
                        (*dungeon)[x][y] = 'G';
                    }
                }
            }
        }

    }
}

void DungeonGenerator::CellularAutomata(Dungeon* dungeon, int fillProbability)
{
    for (int x = 0; x < dungeon->GetWidth(); x++)
    {
        for (int y = 0; y < dungeon->GetHeight(); y++)
        {
            if (rand() %  100 < fillProbability)
            {
                (*dungeon)[x][y] = 'W';
            }
            else
            {
                (*dungeon)[x][y] = 'G';
            }
        }
    }
}

void DungeonGenerator::CellularAutomataIterate(Dungeon* dungeon)
{
    char** temp = new char*[dungeon->GetWidth()];

    for(int x = 0; x < dungeon->GetWidth(); x++)
    {
        temp[x] = new char[dungeon->GetHeight()];
    }

    for (int x = 0; x < dungeon->GetWidth(); x++)
    {
        for (int y = 0; y < dungeon->GetHeight(); y++)
        {
            int neighborsThatAreWalls = 0;
            //Check upper neighbors
            //UL
            if ( (x - 1) > 0 && (y - 1) > 0)
            {
                if ((*dungeon)[x -1][y - 1] == 'W')
                {
                    neighborsThatAreWalls++;
                }
            }
            //U
            if ( (y - 1) > 0)
            {
                if ((*dungeon)[x][y - 1] == 'W')
                {
                    neighborsThatAreWalls++;
                }
            }
            //UR
            if ( (x + 1) < dungeon->GetWidth() && (y - 1) > 0)
            {
                if ((*dungeon)[x + 1][y - 1] == 'W')
                {
                    neighborsThatAreWalls++;
                }
            }
            //Check side neighbors
            //L
            if ( (x - 1) > 0)
            {
                if ((*dungeon)[x - 1][y] == 'W')
                {
                    neighborsThatAreWalls++;
                }
            }
            //R
            if ( (x + 1) < dungeon->GetWidth())
            {
                if ((*dungeon)[x + 1][y] == 'W')
                {
                    neighborsThatAreWalls++;
                }
            }
            //Check lower neighbors
            //LL
            if ( (x - 1) > 0 && (y + 1) < dungeon->GetHeight())
            {
                if ((*dungeon)[x -1][y + 1] == 'W')
                {
                    neighborsThatAreWalls++;
                }
            }
            //L
            if ( (y + 1) < dungeon->GetHeight())
            {
                if ((*dungeon)[x][y + 1] == 'W')
                {
                    neighborsThatAreWalls++;
                }
            }
            //LR
            if ( (x + 1) < dungeon->GetWidth() && (y + 1) < dungeon->GetHeight())
            {
                if ((*dungeon)[x + 1][y + 1] == 'W')
                {
                    neighborsThatAreWalls++;
                }
            }

            temp[x][y] = (*dungeon)[x][y];

            if (temp[x][y] == 'W')
            {
                if (neighborsThatAreWalls < 4)
                {
                    temp[x][y] = 'G';
                }
            }
            else if (temp[x][y] == 'G')
            {
                if (neighborsThatAreWalls > 4)
                {
                    temp[x][y] = 'W';
                }
            }
        }
    }

    dungeon->ReInitialize(temp);
}

void DungeonGenerator::TunnelBetweenRooms(int corridorWidth, Node* rootNode, Dungeon* dungeon)
{
    //All of the rooms in the dungeon.
    vector<Room*> rooms;
    //In order traversal
    stack<Node*> traversalStack;

    if (NULL != rootNode->GetFirstChild())
    {
        traversalStack.push(rootNode->GetSecondChild());
        traversalStack.push(rootNode->GetFirstChild());
    }

    while(traversalStack.size() > 0)
    {
        Node* topNode = traversalStack.top();

        if (NULL != topNode)
        {
            traversalStack.pop();
            if (NULL != topNode->GetSection()->GetRoom())
            {
                rooms.push_back(topNode->GetSection()->GetRoom());
            }

            if (NULL != topNode->GetFirstChild())
            {
                traversalStack.push(topNode->GetSecondChild());
                traversalStack.push(topNode->GetFirstChild());
            }
        }
    }

    qDebug() << "Found " << rooms.size() << " rooms for tunneling.";

    if (rooms.size() > 1)
    {
        for (unsigned int i = 1; i < rooms.size(); i++)
        {
            int startingPointX = 0; //rooms[i]->GetUpperLeftX() + (rooms[i]->GetWidth()/2);
            int startingPointY = 0; //rooms[i]->GetUpperLeftY() + (rooms[i]->GetHeight()/2);
            int endingPointX = 0; //rooms[i - 1]->GetUpperLeftX() + (rooms[i - 1]->GetWidth()/2);
            int endingPointY = 0; //rooms[i - 1]->GetUpperLeftY() + (rooms[i - 1]->GetHeight()/2);

            Room* firstRoom = rooms[i-1];
            Room* secondRoom = rooms[i];

            qDebug() << "First room upper left (" << firstRoom->GetUpperLeftX()
                    << "," << firstRoom->GetUpperLeftY() << ")";

            qDebug() << "Second room upper left (" << secondRoom->GetUpperLeftX()
                    << "," << secondRoom->GetUpperLeftY() << ")";

            //Determine Start/End X
            startingPointX = firstRoom->GetUpperLeftX() + (firstRoom->GetWidth()/2);
            endingPointX = secondRoom->GetUpperLeftX() + (secondRoom->GetWidth()/2);

            //Determine Start/End Y
            startingPointY = firstRoom->GetUpperLeftY() + (firstRoom->GetHeight() / 2);
            endingPointY = secondRoom->GetUpperLeftY() + (secondRoom->GetHeight() / 2);

            qDebug() << "Tunneling, start point ("<< startingPointX << "," << startingPointY
                    <<  ") end point(" << endingPointX << "," << endingPointY << ")";


            int moveIncrementX = 0;
            int moveIncrementY = 0;

            if (startingPointX > endingPointX)
            {
                moveIncrementX = -1;
            }
            else if (startingPointX < endingPointX)
            {
                moveIncrementX = 1;
            }

            if (startingPointY > endingPointY)
            {
                moveIncrementY = -1;
            }
            else if (startingPointY < endingPointY)
            {
                moveIncrementY = 1;
            }

            bool walkingX = true;
            bool walkingY = true;

            do
            {
                if (startingPointX == endingPointX)
                {
                    walkingX = false;
                }

                if (startingPointY == endingPointY)
                {
                    walkingY = false;
                }

                //Tunnel through walls and create floors in areas that are currently void.
                //If the room already is already tiled as part of room, leave it as is and
                //continue to walk.
                if ((*dungeon)[startingPointX][startingPointY] == 'V'
                    || (*dungeon)[startingPointX][startingPointY] == 'W')
                {
                    (*dungeon)[startingPointX][startingPointY] = 'C';

                    int sectionsAdded = 1;// start at 1 for the main corridor
                    int offset = 0;
                    while (sectionsAdded != corridorWidth)
                    {
                        if (0 == sectionsAdded % 2)
                        {
                            if ((*dungeon)[startingPointX + offset][startingPointY] == 'V'
                                || (*dungeon)[startingPointX + offset][startingPointY] == 'W')
                            {
                                (*dungeon)[startingPointX + offset][startingPointY] = 'C';
                            }
                        }
                        else
                        {
                            offset++;
                            if ((*dungeon)[startingPointX - offset][startingPointY] == 'V'
                                || (*dungeon)[startingPointX - offset][startingPointY] == 'W')
                            {
                                (*dungeon)[startingPointX - offset][startingPointY] = 'C';
                            }
                        }
                        sectionsAdded++;
                    }

                }

                if (walkingX)
                {
                    startingPointX += moveIncrementX;
                }

                if (walkingY)
                {
                    startingPointY += moveIncrementY;
                }
            }
            while (walkingX || walkingY);
        }
    }
}
