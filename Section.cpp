#include "Section.h"
#include <stdlib.h>

Section::Section()
{
    upperLeftX = 0;
    upperLeftY = 0;
    room = NULL;
}

Section::~Section()
{
    delete room;
}

Room* Section::GetRoom()
{
    return room;
}

void Section::SetRoom(Room* room)
{
    this->room = room;
}

void Section::SetUpperLeftX(int value)
{
    upperLeftX = value;
}

int Section::GetUpperLeftX()
{
    return upperLeftX;
}

void Section::SetUpperLeftY(int value)
{
    upperLeftY = value;
}

int Section::GetUpperLeftY()
{
    return upperLeftY;
}

int Section::GetHeight()
{
    return length;
}

void Section::SetHeight(int length)
{
    this->length = length;
}

int Section::GetWidth()
{
    return width;
}

void Section::SetWidth(int width)
{
    this->width = width;
}
