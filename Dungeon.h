#ifndef DUNGEON_H
#define DUNGEON_H

class Dungeon
{
public:
    Dungeon(int width, int height);
    ~Dungeon();
    char* operator[] (const int nIndex);
    char GetSymbolAt(int x, int y);
    void ReInitialize(char** dungeon);
    int GetWidth();
    int GetHeight();
    int GetMaxNumberOfRooms();
    void SetMaxNumberOfRooms(int maxNumber);
    int GetMaxRoomWidth();
    int GetMaxRoomHeight();
    int GetMinRoomWidth();
    int GetMinRoomHeight();
    void SetMaxRoomWidth(int value);
    void SetMaxRoomHeight(int value);
    void SetMinRoomWidth(int value);
    void SetMinRoomHeight(int value);

private:
    void Initialize();
    int _fillProbability;
    int _width;
    int _height;
    int maxRoomWidth;
    int maxRoomHeight;
    int minRoomWidth;
    int minRoomHeight;
    int maxNumberOfRooms;
    char** _dungeon;
};

#endif // DUNGEON_H
