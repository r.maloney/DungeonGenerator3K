#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include "ControlPanel.h"
#include "Dungeon.h"

namespace Ui {
    class MainWindow;
}

enum GenerationAlgorithms {CellularAutomata = 0, ConstantSizePartitioning = 1, BinarySpacePartitioning = 2};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void GenerateNewDungeon();
    void Redraw();
    void NewAlgorithmSelected(int newIndex);
    void IterateExistingDungeon();

private:    
    Ui::MainWindow *ui;
    QGraphicsScene* scene;
    ControlPanel* controlPanel;
    Dungeon* dungeon;
    GenerationAlgorithms currentAlgorithm;
};

#endif // MAINWINDOW_H
