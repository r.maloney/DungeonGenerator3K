#include "dungeon.h"

Dungeon::Dungeon(int width, int height) : _width(width), _height(height)
{
    _dungeon = new char*[width];

    for(int x = 0; x < width; x++)
    {
        _dungeon[x] = new char[height];
    }

    Initialize();
}

Dungeon::~Dungeon()
{
    for(int x = 0; x < _width; x++)
    {
        delete [] _dungeon[x];
    }
    delete [] _dungeon;
}

void Dungeon::Initialize()
{
    for(int x = 0; x < _width; x++)
    {
        for(int y = 0; y < _height; y++)
        {
            _dungeon[x][y] = 'V';
        }
    }
}


void Dungeon::ReInitialize(char** dungeon)
{
    delete _dungeon;
    _dungeon = dungeon;
}

char Dungeon::GetSymbolAt(int x, int y)
{
    return _dungeon[x][y];
}

char* Dungeon::operator[] (const int nIndex)
{
    return _dungeon[nIndex];
}

int Dungeon::GetWidth()
{
    return _width;
}

int Dungeon::GetHeight()
{
    return _height;
}

int Dungeon::GetMaxNumberOfRooms()
{
    return maxNumberOfRooms;
}

void Dungeon::SetMaxNumberOfRooms(int maxNumber)
{
    maxNumberOfRooms = maxNumber;
}


int Dungeon::GetMaxRoomWidth()
{
    return maxRoomWidth;
}

int Dungeon::GetMaxRoomHeight()
{
    return maxRoomHeight;
}

int Dungeon::GetMinRoomWidth()
{
    return minRoomWidth;
}

int Dungeon::GetMinRoomHeight()
{
    return minRoomHeight;
}

void Dungeon::SetMaxRoomWidth(int value)
{
    maxRoomWidth = value;
}

void Dungeon::SetMaxRoomHeight(int value)
{
    maxRoomHeight = value;
}

void Dungeon::SetMinRoomWidth(int value)
{
    minRoomWidth = value;
}

void Dungeon::SetMinRoomHeight(int value)
{
    minRoomHeight = value;
}
