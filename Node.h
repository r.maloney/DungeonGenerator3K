#ifndef NODE_H
#define NODE_H
#include "Section.h"

class Node
{
public:
    Node();
    ~Node();
    Section* GetSection();
    void SetSection(Section*);

    Node* GetFirstChild();
    void SetFirstChild(Node*);

    Node* GetSecondChild();
    void SetSecondChild(Node*);

    bool IsVerticalCut();
    void SetVerticalCut(bool);

private:
    Section* section;
    Node* firstChild;
    Node* secondChild;
    bool verticalCut;
};

#endif // NODE_H
