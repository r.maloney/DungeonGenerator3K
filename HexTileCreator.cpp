#define _USE_MATH_DEFINES
#include "HexTileCreator.h"
#include "math.h"

HexTileCreator::HexTileCreator()
{
}

int HexTileCreator::DetermineWidth(int sideLength)
{
    int _r = floor(cos(30 * M_PI / 180) * sideLength);
    return 2 * _r;
}

int HexTileCreator::GetHorizontalOffset(int sideLength)
{
    return floor(cos(30 * M_PI / 180) * sideLength);
}

int HexTileCreator::GetVerticalOffset(int sideLength)
{
    return floor(sin(30.0 * M_PI / 180.0) * (1.0 * sideLength));
}

QVector<QPointF> HexTileCreator::GeneratePointsForHex(int centerX, int centerY, int sideLength)
{
    QVector<QPointF> vector;
    int _h = floor(sin(30 * M_PI / 180) * sideLength);
    int _r = floor(cos(30 * M_PI / 180) * sideLength);
    int _height = sideLength + (2 * _h);
    int _width = 2 * _r;

    //Add top point.
    QPointF topPoint;
    topPoint.setX(centerX);
    topPoint.setY(centerY - (_height / 2));
    vector.append(topPoint);

    //Add first left point
    QPointF leftPoint;
    leftPoint.setX(centerX - (_width / 2));
    leftPoint.setY(centerY - (sideLength / 2));
    vector.append(leftPoint);

    //Add second left point
    QPointF leftPoint2;
    leftPoint2.setX(centerX - (_width / 2));
    leftPoint2.setY(centerY + (sideLength / 2));
    vector.append(leftPoint2);

    //Add bottom point
    QPointF bottomPoint;
    bottomPoint.setX(centerX);
    bottomPoint.setY(centerY + (_height / 2));
    vector.append(bottomPoint);

    //Add bottom right point
    QPointF bottomRightPoint;
    bottomRightPoint.setX(centerX + (_width / 2));
    bottomRightPoint.setY(centerY +(sideLength / 2));
    vector.append(bottomRightPoint);

    //Add top right point
    QPointF topRightPoint;
    topRightPoint.setX(centerX + (_width / 2));
    topRightPoint.setY(centerY - (sideLength / 2));
    vector.append(topRightPoint);

    return vector;
}
