#ifndef DUNGEONGENERATOR_H
#define DUNGEONGENERATOR_H
#include "Dungeon.h"
#include "Node.h"
#include "Section.h"
class DungeonGenerator
{
public:
    static void CellularAutomata(Dungeon* dungeon, int fillProbability);
    static void CellularAutomataIterate(Dungeon* dungeon);
    static void ConstantSizePartitioning(Dungeon* dungeon);
    static void BinarySpacePartitioning(Dungeon* dungeon);


private:
    DungeonGenerator();

    //Split a section into two halves.
    static void SplitSection(Node* parentNode, int minRoomWidth, int minRoomHeight);

    //Add rooms to a dungeon based on a tree populated with section splits.
    static void PopulateDungeonFromTree(Node* rootNode, Dungeon* dungeon);

    //Add a room to a given section within a dungeon.  The overloaded version of the
    //method allows the caller to specify an offset or "buffer" around the room within the section.
    static bool PopulateSectionWithRoom(Section* section, Dungeon* dungeon);
    static bool PopulateSectionWithRoom(Section* section, Dungeon* dungeon, int offsetRoomFromSection);

    static void PopulateDungeonFromSection(Section* section, Dungeon* dungeon);
    static void PopulateDungeonFromSection(Section* section, Dungeon* dungeon, int offsetRoomFromSection);

    //Draw tunnels between rooms
    static void TunnelBetweenRooms(int corridorWidth, Node* rootNode, Dungeon* dungeon);
};

#endif // DUNGEONGENERATOR_H
