#ifndef SECTION_H
#define SECTION_H
#include "Room.h"

class Section
{
public:
    Section();
    ~Section();

    Room* GetRoom();
    void SetRoom(Room* room);

    void SetUpperLeftX(int value);
    int GetUpperLeftX();

    void SetUpperLeftY(int value);
    int GetUpperLeftY();

    int GetHeight();
    void SetHeight(int length);

    int GetWidth();
    void SetWidth(int width);

private:
    Room* room;
    int upperLeftX;
    int upperLeftY;
    int width;
    int length;
};

#endif // SECTION_H
