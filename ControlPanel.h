#ifndef CONTROLPANEL_H
#define CONTROLPANEL_H
#include <QObject>

class ControlPanel : public QObject
{
    Q_OBJECT

public:
    ControlPanel(QObject *parent = 0);
    bool IsDrawHexSelected();
    void SetDrawHex(bool value);
    int GetTileWidth();
    int GetSideLength();
    void SetSideLength(int length);
    int GetMapHeight();
    void SetMapHeight(int height);
    int GetMapWidth();
    void SetMapWidth(int width);
    int GetMaxNumberOfRooms();
    void SetMaxNumberOfRooms(int maxNumber);
    int GetFillProbability();
    void SetFillProbability(int fillProbability);
    int GetMaxRoomWidth();
    int GetMaxRoomHeight();
    int GetMinRoomWidth();
    int GetMinRoomHeight();
    void SetMaxRoomWidth(int value);
    void SetMaxRoomHeight(int value);
    void SetMinRoomWidth(int value);
    void SetMinRoomHeight(int value);

public slots:
    void ToggleDrawingType(bool value);
    void UpdateFillProbability(int fillProbability);
    void UpdateMaxRoomWidth(int value);
    void UpdateMaxRoomHeight(int value);
    void UpdateMinRoomWidth(int value);
    void UpdateMinRoomHeight(int value);
    void UpdateMapWidth(int value);
    void UpdateMapHeight(int value);
    void UpdateMaxNumberOfRooms(int value);

private:
    bool drawHex;
    int sideLength;
    int mapHeight;
    int mapWidth;
    int maxRoomWidth;
    int maxRoomHeight;
    int minRoomWidth;
    int minRoomHeight;
    int maxNumberOfRooms;
    int fillProbability;
};

#endif // CONTROLPANEL_H
