#include "Room.h"

Room::Room()
{
}

void Room::SetWidth(int value)
{
    width = value;
}

void Room::SetHeight(int value)
{
    height = value;
}

int Room::GetWidth()
{
    return width;
}

int Room::GetHeight()
{
    return height;
}

int Room::GetUpperLeftX(void)
{
    return x;
}

void Room::SetUpperLeftX(int x)
{
    this->x = x;
}

int Room::GetUpperLeftY(void)
{
    return y;
}

void Room::SetUpperLeftY(int y)
{
    this->y = y;
}
