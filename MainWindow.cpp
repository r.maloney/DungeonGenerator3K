#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Dungeon.h"
#include <QGraphicsTextItem>
#include <QGraphicsRectItem>
#include <QGraphicsPolygonItem>
#include <QString>
#include <QDebug>
#include "DungeonGenerator.h"
#include "HexTileCreator.h"


MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::MainWindow)
{
    scene = NULL;
    dungeon = NULL;
    controlPanel = new ControlPanel();

    ui->setupUi(this);

    //Build combo box
    ui->algorithms->insertItem(0, QString("Cellular Automata"));
    ui->algorithms->insertItem(1, QString("Constant Size Partitioning"));
    ui->algorithms->insertItem(2, QString("Binary Space Partitioning"));
    ui->algorithms->setCurrentIndex(-1);

    //Defaults
    controlPanel->SetSideLength(20);
    controlPanel->SetDrawHex(true);

    //Toggle drawing types
    QObject::connect(ui->drawTilesAsHex, SIGNAL(toggled(bool)),
                     controlPanel, SLOT(ToggleDrawingType(bool)));

    //Regenerate dungeon
    QObject::connect(ui->generateButton, SIGNAL(pressed(void)),
                     this, SLOT(GenerateNewDungeon(void)));

    //Iterate an existing dungeon
    QObject::connect(ui->iterateButton, SIGNAL(pressed(void)),
                     this, SLOT(IterateExistingDungeon(void)));

    //Choose new algorithm
    QObject::connect(ui->algorithms, SIGNAL(currentIndexChanged(int)),
                     this, SLOT(NewAlgorithmSelected(int)));

    //Update fill probability
    QObject::connect(ui->fillPercentage, SIGNAL(valueChanged(int)),
                     controlPanel, SLOT(UpdateFillProbability(int)));

    //Update Map Width
    QObject::connect(ui->mapWidthSpinBox, SIGNAL(valueChanged(int)),
                     controlPanel, SLOT(UpdateMapWidth(int)));

    //Update Map Height
    QObject::connect(ui->mapHeightSpinBox, SIGNAL(valueChanged(int)),
                     controlPanel, SLOT(UpdateMapHeight(int)));

    //Update Max Room Width
    QObject::connect(ui->maxRoomWidthSpinBox, SIGNAL(valueChanged(int)),
                     controlPanel, SLOT(UpdateMaxRoomWidth(int)));

    //Update Max Room Height
    QObject::connect(ui->maxRoomHeightSpinBox, SIGNAL(valueChanged(int)),
                     controlPanel, SLOT(UpdateMaxRoomHeight(int)));

    //Update Min Room Width
    QObject::connect(ui->minRoomHeightSpinBox, SIGNAL(valueChanged(int)),
                     controlPanel, SLOT(UpdateMinRoomHeight(int)));

    //Update Min Room Height
    QObject::connect(ui->minRoomWidthSpinBox, SIGNAL(valueChanged(int)),
                     controlPanel, SLOT(UpdateMinRoomWidth(int)));

    //Update Max Room Number
    QObject::connect(ui->maxNumberOfRooms, SIGNAL(valueChanged(int)),
                     controlPanel, SLOT(UpdateMaxNumberOfRooms(int)));

    //All redraw events
    QObject::connect(ui->drawTilesAsHex, SIGNAL(toggled(bool)),
                     this, SLOT(Redraw(void)));
    QObject::connect(ui->drawTilesAsSquare, SIGNAL(toggled(bool)),
                     this, SLOT(Redraw(void)));
    QObject::connect(ui->algorithms, SIGNAL(currentIndexChanged(int)),
                     this, SLOT(Redraw(void)));
}

void MainWindow::GenerateNewDungeon()
{
    ui->generateButton->setEnabled(false);
    delete dungeon;
    dungeon = NULL;

    dungeon = new Dungeon(controlPanel->GetMapWidth(), controlPanel->GetMapHeight());
    dungeon->SetMaxNumberOfRooms(controlPanel->GetMaxNumberOfRooms());
    dungeon->SetMaxRoomWidth(controlPanel->GetMaxRoomWidth());
    dungeon->SetMaxRoomHeight(controlPanel->GetMaxRoomHeight());
    dungeon->SetMinRoomWidth(controlPanel->GetMinRoomWidth());
    dungeon->SetMinRoomHeight(controlPanel->GetMinRoomHeight());

    switch (currentAlgorithm)
    {
    case CellularAutomata:
        DungeonGenerator::CellularAutomata(dungeon, controlPanel->GetFillProbability());
        break;
    case ConstantSizePartitioning:
        DungeonGenerator::ConstantSizePartitioning(dungeon);
        break;
    case BinarySpacePartitioning:
        DungeonGenerator::BinarySpacePartitioning(dungeon);
        break;
    };

    Redraw();
    ui->generateButton->setEnabled(true);
}

void MainWindow::Redraw()
{
    if (NULL == scene)
    {
        scene = new QGraphicsScene(ui->graphicsView);
    }

    scene->clear();

    int tileWidth = controlPanel->GetTileWidth();

    for (int y = 0; y < controlPanel->GetMapHeight(); y++)
    {
        for (int x = 0; x < controlPanel->GetMapWidth(); x++)
        {
            int drawX = x * tileWidth;
            int drawY = y * tileWidth;
            char code = (*dungeon)[x][y];

            if (controlPanel->IsDrawHexSelected())
            {
                if (y % 2 == 0)
                {
                    drawX += HexTileCreator::GetHorizontalOffset(controlPanel->GetSideLength());
                }

                if (y > 0)
                {
                    drawY -= (y * HexTileCreator::GetVerticalOffset(controlPanel->GetSideLength()));
                }

                QPolygonF poly(HexTileCreator::GeneratePointsForHex(drawX, drawY, controlPanel->GetSideLength()));
                QGraphicsPolygonItem* polyItem = scene->addPolygon(poly, QPen(Qt::black), QBrush(Qt::black));

                switch (code)
                {
                case 'V':
                    polyItem->setBrush(QBrush(Qt::blue));
                    break;
                case 'W':
                    polyItem->setBrush(QBrush(Qt::gray));
                    break;
                case 'G':
                    polyItem->setBrush(QBrush(Qt::darkGreen));
                    break;
                };
            }
            else
            {
                QGraphicsRectItem* rectItem = scene->addRect(drawX, drawY, tileWidth, tileWidth, QPen(Qt::black), QBrush(Qt::black));

                switch (code)
                {
                case 'V':
                    rectItem->setBrush(QBrush(Qt::blue));
                    break;
                case 'W':
                    rectItem->setBrush(QBrush(Qt::gray));
                    break;
                case 'G':
                    rectItem->setBrush(QBrush(Qt::darkGreen));
                    break;
                };
            }
        }
    }

    ui->graphicsView->setScene(scene);
    ui->graphicsView->show();
}

void MainWindow::NewAlgorithmSelected(int newIndex)
{
    switch(newIndex)
    {
    case 0:
        currentAlgorithm = CellularAutomata;
        ui->iterateButton->setEnabled(true);
        ui->fillPercentage->setEnabled(true);
        ui->maxNumberOfRooms->setEnabled(false);
        break;
    case 1:
        currentAlgorithm = ConstantSizePartitioning;
        ui->iterateButton->setEnabled(false);
        ui->fillPercentage->setEnabled(false);
        ui->maxNumberOfRooms->setEnabled(true);
        break;
    case 2:
        currentAlgorithm = BinarySpacePartitioning;
        ui->iterateButton->setEnabled(false);
        ui->fillPercentage->setEnabled(false);
        ui->maxNumberOfRooms->setEnabled(true);
        break;
    };

    GenerateNewDungeon();
}

void MainWindow::IterateExistingDungeon()
{
    if (CellularAutomata == currentAlgorithm)
    {
        DungeonGenerator::CellularAutomataIterate(dungeon);
        Redraw();
    }
    else if (ConstantSizePartitioning == currentAlgorithm)
    {
        DungeonGenerator::ConstantSizePartitioning(dungeon);
        Redraw();
        //TODO: Report status
    }
}

MainWindow::~MainWindow()
{
    delete scene;
    delete controlPanel;
    delete ui;
}
