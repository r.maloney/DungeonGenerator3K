#ifndef HEXTILECREATOR_H
#define HEXTILECREATOR_H

#include <QVector>
#include <QPointF>

class HexTileCreator
{
public:
    static int DetermineWidth(int sideLength);
    static int GetHorizontalOffset(int sideLength);
     static int GetVerticalOffset(int sideLength);
    static QVector<QPointF> GeneratePointsForHex(int centerX, int centerY, int sideLength);
private:
    HexTileCreator();
};

#endif // HEXTILECREATOR_H
