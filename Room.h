#ifndef ROOM_H
#define ROOM_H

class Room
{
public:
    Room();
    void SetWidth(int value);
    void SetHeight(int value);

    int GetWidth();
    int GetHeight();

    int GetUpperLeftX(void);
    void SetUpperLeftX(int);

    int GetUpperLeftY(void);
    void SetUpperLeftY(int);

private:
    int width;
    int height;
    int x;
    int y;
};

#endif // ROOM_H
