#include "Node.h"
#include <stdlib.h>

Node::Node()
{
    this->section = NULL;
    this->firstChild = NULL;
    this->secondChild = NULL;
    verticalCut = true;
}

Node::~Node()
{
    delete this->section;
    delete this->firstChild;
    delete this->secondChild;
}

Section* Node::GetSection()
{
    return section;
}

void Node::SetSection(Section* section)
{
    this->section = section;
}

Node* Node::GetFirstChild()
{
    return firstChild;
}

void Node::SetFirstChild(Node* firstChild)
{
    this->firstChild = firstChild;
}

Node* Node::GetSecondChild()
{
    return secondChild;
}

void Node::SetSecondChild(Node* secondChild)
{
    this->secondChild = secondChild;
}

bool Node::IsVerticalCut()
{
    return verticalCut;
}

void Node::SetVerticalCut(bool verticalCut)
{
    this->verticalCut = verticalCut;
}
