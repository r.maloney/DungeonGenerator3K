#include "ControlPanel.h"
#include "HexTileCreator.h"
#include <QDebug>

ControlPanel::ControlPanel(QObject *parent): QObject(parent)
{
    drawHex = true;
    sideLength = 0;
    mapHeight = 0;
    mapWidth = 0;
    maxRoomWidth = 0;
    maxRoomHeight = 0;
    minRoomWidth = 0;
    minRoomHeight = 0;
}

int ControlPanel::GetMaxNumberOfRooms()
{
    return maxNumberOfRooms;
}

void ControlPanel::SetMaxNumberOfRooms(int maxNumber)
{
    maxNumberOfRooms = maxNumber;
}

void ControlPanel::UpdateMaxNumberOfRooms(int value)
{
    maxNumberOfRooms = value;
}

void ControlPanel::UpdateMapWidth(int value)
{
    mapWidth = value;
}

void ControlPanel::UpdateMapHeight(int value)
{
    mapHeight = value;
}

int ControlPanel::GetMaxRoomWidth()
{
    return maxRoomWidth;
}

int ControlPanel::GetMaxRoomHeight()
{
    return maxRoomHeight;
}

int ControlPanel::GetMinRoomWidth()
{
    return minRoomWidth;
}

int ControlPanel::GetMinRoomHeight()
{
    return minRoomHeight;
}

void ControlPanel::SetMaxRoomWidth(int value)
{
    maxRoomWidth = value;
}

void ControlPanel::SetMaxRoomHeight(int value)
{
    maxRoomHeight = value;
}

void ControlPanel::SetMinRoomWidth(int value)
{
    minRoomWidth = value;
}

void ControlPanel::SetMinRoomHeight(int value)
{
    minRoomHeight = value;
}

void ControlPanel::UpdateMaxRoomWidth(int value)
{
    maxRoomWidth = value;
}

void ControlPanel::UpdateMaxRoomHeight(int value)
{
    maxRoomHeight = value;
}

void ControlPanel::UpdateMinRoomWidth(int value)
{
    minRoomWidth = value;
}

void ControlPanel::UpdateMinRoomHeight(int value)
{
    minRoomHeight = value;
}

bool ControlPanel::IsDrawHexSelected()
{
    return drawHex;
}

void ControlPanel::SetDrawHex(bool value)
{
    drawHex = value;
}

int ControlPanel::GetTileWidth()
{
    int tileWidth = 0;

    if (drawHex)
    {
        tileWidth = HexTileCreator::DetermineWidth(sideLength);
    }
    else
    {
        tileWidth = sideLength;
    }

    return tileWidth;
}

int ControlPanel::GetSideLength()
{
    return sideLength;
}

void ControlPanel::SetSideLength(int length)
{
    sideLength = length;
}

int ControlPanel::GetMapHeight()
{
    return mapHeight;
}

void ControlPanel::SetMapHeight(int height)
{
    mapHeight = height;
}

int ControlPanel::GetMapWidth()
{
    return mapWidth;
}

void ControlPanel::SetMapWidth(int width)
{
    mapWidth = width;
}

void ControlPanel::ToggleDrawingType(bool value)
{
    drawHex = !drawHex;
}

int ControlPanel::GetFillProbability()
{
    return fillProbability;
}

void ControlPanel::SetFillProbability(int fillProbability)
{
    this->fillProbability = fillProbability;
}

void ControlPanel::UpdateFillProbability(int fillProbability)
{
    this->fillProbability = fillProbability;
}
